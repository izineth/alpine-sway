#!/bin/sh

AUSER=""

adduser $AUSER wheel
adduser $AUSER input
adduser $AUSER video
adduser $AUSER audio

setup-xorg-base
apk add dbus xdg-desktop-portal xdg-desktop-portal-wlr linux-firmware wireless-tools util-linux \
pciutils usbutils coreutils binutils findutils grep iproute2 \
alsa-utils alsa-utils-doc alsa-lib alsaconf alsa-ucm-conf bluez mako \
python3 network-manager-applet kanshi \
curl zsh gawk grim slurp bash bash-doc bash-completion \
terminus-font ttf-inconsolata font-misc-misc ttf-font-awesome \
udisks2 udisks2-doc mesa-dri-gallium \
flatpak rofi-wayland waybar sway swaybg swayidle swaylock swaylockd foot elogind polkit-elogind autotiling \
seatd 

apk add eudev
setup-devd udev

mkdir -p /home/$AUSER/.config

rc-update add seatd
rc-service seatd start
adduser $AUSER seat
rc-service dbus start
rc-update add dbus
rc-service alsa start
rc-update add alsa
